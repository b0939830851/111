/*
 * 定義一個 scene，用成員變數儲存 scene 上面的物件
 * override preload, create, update 函式
 */
class Scene extends Phaser.Scene {
  /*********************************************************************************************************************** */
  constructor() {
    super();
  }

  /*********************************************************************************************************************** */
  preload() {
    this.load.image("Background", "assets/Background.png");
    this.load.image("coin", "assets/coin.png");
    this.load.image("eaten", "assets/eaten.png");
    this.load.image("ground", "assets/ground.png");
    this.load.image("play", "assets/play.png");
    this.load.image("gameover", "assets/gameover.png");
    this.load.image("win", "assets/win.png");
    this.load.image("nohealth", "assets/nohealth.png");
    this.load.image("onehealth", "assets/onehealth.png");
    this.load.image("twohealth", "assets/twohealth.png");
    this.load.image("threehealth", "assets/threehealth.png");
    this.load.image("fourhealth", "assets/fourhealth.png");
    this.load.image("fullhealth", "assets/fullhealth.png");
    this.load.spritesheet("dino", "assets/dinosprites.png", {
      frameWidth: 46.666666,
      frameHeight: 37,
    });
  }

  /************************************************************************************************************************/
  create() {
    //背景
    this.add.image(250, 144, 'Background');
    this.platforms=this.physics.add.staticGroup();
    let p=this.platforms.create(250, 270,"ground");
    p.refreshBody();

    //玩家
    this.player=new Player(this, 250, 100);
    this.physics.add.collider(this.player, this.platforms);

    //鍵盤控制
    this.cursors=this.input.keyboard.createCursorKeys();

    // 金幣群組
    this.coins = this.physics.add.group();
    this.physics.add.collider(this.coins, this.platforms, this.coinHitGround, null, this);

    //分數的改變
    this.score=0;
    this.scoreText = this.add.text(25, 23, "0", {
      fontSize: '30px',
      fill: '#fff',
      fontStyle: 'bold'
    });

    //吃到金幣
    this.physics.add.overlap(this.player, this.coins, (p, coin) => {
      coin.disableBody(true, true);
      this.score++;
      this.eat = true;

      // 檢查獲勝條件
      if (this.score == 15) {
        this.gameWon();
      }
    });
    
    // 創建血量條
    this.health = 5;
    this.curhealth = this.add.image(this.sys.game.config.width-80, 41, 'fullhealth');
    this.curhealth.setScale(0.75);

    //動畫
    this.anims.create({
      key: "walk",
      frames: this.anims.generateFrameNumbers("dino", { start: 3, end: 10 }),
      frameRate: 10,
      repeat: -1
    });
    this.anims.create({
      key: "idle",
      frames: [{ key: "dino", frame: 0 }]
    });
    this.anims.create({
      key: "eaten",
      frames: [{ key: "dino", frame: 15 }]
    });


    //添加 "Play" 圖像並設置點擊事件
    this.playButton = this.add.image(this.sys.game.config.width/2, this.sys.game.config.height/2, 'play').setInteractive();
    //this.playButton.setScale(0.8);
    this.playButton.on('pointerdown', this.startGame, this);
  }

  startGame() {
    this.playButton.destroy(); //刪除 "Play" 圖像
    this.gameStarted = true; //標記遊戲為已開始

    //每秒生成一個金幣
    this.coinEvent = this.time.addEvent({
      delay: 1300,
      callback: this.spawnCoin,
      callbackScope: this,
      loop: true
    });
  }
  spawnCoin() {
    let x = Phaser.Math.Between(20, this.sys.game.config.width-18); //随機生成x座標
    let coin = new Coin(this, x, 0);
    this.coins.add(coin);
  }

  coinHitGround(coin, ground) {
    coin.disableBody(true, true); // 禁用金幣
    coin.destroy();               // 銷毀金幣
    this.health--; // 減少血量
    this.curhealth.destroy();
    //更新血量
    if (this.health == 4) {
      this.curhealth = this.add.image(this.sys.game.config.width-80, 41, 'fourhealth');
      this.curhealth.setScale(0.75);
    }
    else if (this.health == 3){
      this.curhealth = this.add.image(this.sys.game.config.width-80, 41, 'threehealth');
      this.curhealth.setScale(0.75);
    }
    else if (this.health == 2){
      this.curhealth = this.add.image(this.sys.game.config.width-80, 41, 'twohealth');
      this.curhealth.setScale(0.75);
    }
    else if (this.health == 1){
      this.curhealth = this.add.image(this.sys.game.config.width-80, 41, 'onehealth');
      this.curhealth.setScale(0.75);
    }
    else if (this.health == 0){
      this.curhealth = this.add.image(this.sys.game.config.width-80, 41, 'nohealth');
      this.curhealth.setScale(0.75);
      this.gameOver();
    }
  }

gameOver() {
  this.gameStarted = false;

  // 停止金幣掉落
    this.coinEvent.remove(false);

    let gameOverImage = this.add.image(this.sys.game.config.width/2, this.sys.game.config.height/2.6, 'gameover');
    gameOverImage.setScale(0.75); // 設置縮放比例
    this.playButton = this.add.image(this.sys.game.config.width/2, this.sys.game.config.height/2 + 33, 'play').setInteractive();
    this.playButton.setScale(0.55);
    this.playButton.on('pointerdown', () => {
      this.scene.restart(); // 重新開始遊戲
    });
}

gameWon() {
  this.gameStarted = false;

  // 停止金幣掉落
  this.coinEvent.remove(false);

  this.add.image(this.sys.game.config.width/2, this.sys.game.config.height/2, 'win');
}


  /*********************************************************************************************************************** */
  update() {
    //如果遊戲未開始，不執行更新邏輯
    if (!this.gameStarted) {
      return; 
    }

    //鍵盤控制
    if (this.cursors.left.isDown) {
      this.player.setVelocityX(-190);
      this.player.anims.play('walk', true);
      this.player.flipX=true;
    } else if (this.cursors.right.isDown) {
      this.player.setVelocityX(190);
      this.player.anims.play('walk', true);
      this.player.flipX=false;
    } else {
      this.player.setVelocityX(0);
      this.player.anims.play('idle', true);
    }

    //播放吃到金幣的動畫
    if (this.eat){
      this.player.anims.play('eaten', true);
      this.eat=false;
    }

    //跳躍控制
    if (this.cursors.up.isDown && this.player.body.touching.down) {
      this.player.setVelocityY(-130);
    }

    //分數更新
    this.scoreText.setText(this.score);
  }
}
